var express = require('express');
var app 	= express();
var port 	= 	process.env.PORT || 3000;
var bodyParser = require('body-parser');
var request = require('request');
var path = require('path');


app.use(express.static(__dirname + '/public'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// set the view engine to ejs
app.set('view engine', 'ejs');


var router = express.Router();


router.use(function (req, res, next) {
	console.log('.. so it begins .. ');
	next();
});

router.route('/')
	
	.get(function (req, res) {

		// res.json({ message: ' route is hit!'});
		// res.sendfile(path.join(__dirname, '/index.html'));
		res.render('index.ejs');


	});
	

router.route('/postrequest')
	
	.post(function (req, res) {

		var movieTitle = req.body.name;

		request('http://www.omdbapi.com/?t=' + movieTitle + '&r=json', function (error, response, body) {

			if (!error && response.statusCode == 200) {

				var data = JSON.parse(body);

				// console.log(JSON.stringify(response ,null, '\t'))
				// console.log(JSON.stringify(body ,null, '\t'))

		        res.render('viewposter.ejs',{
		        	title: data["Title"],
		        	year: data["Year"],
		        	runtime: data["Runtime"],
		        	posterurl: data["Poster"],
		        	summary: data["Plot"],
		        	genre: data["Genre"],
		        	director: data["Director"]
		        });

		    }

		});

	});
		

// router.route('/viewData')
	
// 	.get(function (req, res) {

// 		// ** returns index.html*** res.sendfile(path.join(__dirname, '/index.html'));
// 		res.json({ message: ' /viewData route is hit!'});

// 	});
	


app.use('/', router);

app.listen(port);

console.log('Magic happens.......' + port);